﻿using System.Linq;
using System.Threading.Tasks;
using LentaGazetaRSS.Services;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

namespace LentaGazetaRSSTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public async Task CheckRssParser_IsNotNull()
        {
            var lentaParser = new TestRssParser();
            var feed = await lentaParser.GetFeed();
            Assert.IsNotNull(feed);
        }

        [TestMethod]
        public async Task CheckRssParser_IsHasAnyItems()
        {
            var lentaParser = new TestRssParser();
            var feed = await lentaParser.GetFeed();

            Assert.IsNotNull(feed.FirstOrDefault());
        }

        [TestMethod]
        public async Task CheckRssParser_IsHasCorrectSourceName()
        {
            var lentaParser = new TestRssParser();
            var feed = await lentaParser.GetFeed();

            Assert.AreEqual("Lenta.Ru", feed.First().Name);
        }

        [TestMethod]
        public async Task CheckRssParser_IsHasTitle()
        {
            var lentaParser = new TestRssParser();
            var feed = await lentaParser.GetFeed();

            Assert.IsFalse(string.IsNullOrEmpty(feed.First().Title));
        }

        [TestMethod]
        public async Task CheckRssParser_IsHasDescription()
        {
            var lentaParser = new TestRssParser();
            var feed = await lentaParser.GetFeed();

            Assert.IsFalse(string.IsNullOrEmpty(feed.First().Description));
        }

        [TestMethod]
        public async Task CheckRssParser_IsHasCorrectCountOfItems()
        {
            var lentaParser = new TestRssParser();
            var feed = await lentaParser.GetFeed();

            Assert.AreEqual(200, feed.Count());
        }
    }
}