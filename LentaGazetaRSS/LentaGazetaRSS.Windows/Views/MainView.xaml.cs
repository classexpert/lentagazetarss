﻿using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// Шаблон элемента пустой страницы задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234238

namespace LentaGazetaRSS.Views
{
    /// <summary>
    ///     Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainView : Page
    {
        public MainView()
        {
            this.NavigationCacheMode = NavigationCacheMode.Enabled;
            InitializeComponent();
        }
    }
}