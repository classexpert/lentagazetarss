﻿using LentaGazetaRSS.Models.RSS;

namespace LentaGazetaRSS.Models
{
    public class LentaRssItem : BaseRssItem
    {
        public override string Name
        {
            get { return "Lenta.Ru"; }
        }
    }
}