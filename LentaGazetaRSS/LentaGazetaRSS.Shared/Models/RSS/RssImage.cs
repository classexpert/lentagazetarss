﻿using System.Xml.Serialization;

namespace LentaGazetaRSS.Models.RSS
{
    public class RssImage
    {
        [XmlElement("url")]
        public string Url { get; set; }

        [XmlElement("link")]
        public string Link { get; set; }

        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("width")]
        public int Width { get; set; }

        [XmlElement("height")]
        public int Height { get; set; }
    }
}
