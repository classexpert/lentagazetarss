﻿using System.Xml.Serialization;

namespace LentaGazetaRSS.Models.RSS
{
    public class RssEnclosure
    {
        // <enclosure url="http://icdn.lenta.ru/images/2015/07/27/23/20150727232307488/pic_ac292a11562104749b59b3bf2e3aa350.jpg" length="27235" type="image/jpeg"/>
        [XmlAttribute("url")]
        public string Url { get; set; }

        [XmlAttribute("length")]
        public int Length { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }

}
