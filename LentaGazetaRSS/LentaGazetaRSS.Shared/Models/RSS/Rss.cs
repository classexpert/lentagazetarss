﻿using System.Xml.Serialization;

namespace LentaGazetaRSS.Models.RSS
{
    [XmlRoot("rss", IsNullable = false)]
    public class Rss<T> where T : IRssItem
    {
        [XmlElement("channel")]
        public СhannelCollection<T> Collection { get; set; }
    }
}
