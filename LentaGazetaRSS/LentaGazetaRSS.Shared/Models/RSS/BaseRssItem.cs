﻿using System;
using System.Xml.Serialization;

namespace LentaGazetaRSS.Models.RSS
{
    public abstract class BaseRssItem : IRssItem
    {
        public abstract string Name { get; }

        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("guid")]
        public string Guid { get; set; }

        [XmlElement("link")]
        public string Link { get; set; }

        [XmlElement("description")]
        public string Description { get; set; }

        [XmlElement("category")]
        public string Category { get; set; }

        [XmlElement("author")]
        public string Author { get; set; }

        [XmlElement("pubDate")]
        public string PubDate { get; set; }

        [XmlElement(ElementName = "enclosure")]
        public RssEnclosure Enclosure { get; set; }

        [XmlIgnore]
        public DateTime Date
        {
            get
            {
                DateTime date;
                DateTime.TryParse(PubDate, out date);
                return date;
            }
        }
        public string ImageUrl
        {
            get
            {
                if (Enclosure != null)
                    return Enclosure.Url;
                return null;
            }
        }
    }
}