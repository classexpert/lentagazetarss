﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LentaGazetaRSS.Models.RSS
{
    public class СhannelCollection<T> where T : IRssItem
    {
        [XmlElement("link")]
        public string Link { get; set; }

        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("description")]
        public string Description { get; set; }

        [XmlElement("pubDate")]
        public string PublishDate { get; set; }

        [XmlElement("language")]
        public string Lang { get; set; }

        [XmlElement("copyright")]
        public string Copyright { get; set; }

        [XmlElement("webMaster")]
        public string WebMaster { get; set; }

        [XmlElement("ttl")]
        public int Ttl { get; set; }

        [XmlElement("image")]
        public RssImage LogoImageInfo { get; set; }

        [XmlElement("item")]
        public List<T> Items { get; set; }
    }
}