﻿using System;

namespace LentaGazetaRSS.Models.RSS
{
    public interface IRssItem
    {
        string Title { get; set; }
        string Guid { get; set; }
        string Link { get; set; }
        string Description { get; set; }
        string Category { get; set; }
        string Author { get; set; }
        string PubDate { get; set; }
        RssEnclosure Enclosure { get; set; }
        string Name { get; }
        string ImageUrl { get; }

        DateTime Date { get;}
    }
}