﻿using LentaGazetaRSS.Models.RSS;

namespace LentaGazetaRSS.Models
{
    public class GazetaRssItem : BaseRssItem
    {
        public override string Name
        {
            get { return "Gazeta.Ru"; }
        }
    }
}