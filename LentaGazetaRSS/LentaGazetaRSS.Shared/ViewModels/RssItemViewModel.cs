﻿using Caliburn.Micro;
using LentaGazetaRSS.Models.RSS;

namespace LentaGazetaRSS.ViewModels
{
    public class RssItemViewModel : Screen
    {
        private IRssItem _parameter;
        private readonly INavigationService _navigationService;

        public IRssItem Parameter
        {
            get { return _parameter; }
            set
            {
                _parameter = value;
                NotifyOfPropertyChange(() => Parameter);
            }
        }

        public void Back()
        {
            _navigationService.GoBack();
        }

        public RssItemViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }
    }
}