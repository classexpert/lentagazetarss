﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Windows.UI.Xaml.Controls;
using Windows.UI.Popups;
using Caliburn.Micro;
using LentaGazetaRSS.Extensions;
using LentaGazetaRSS.Models.RSS;
using LentaGazetaRSS.Services;

namespace LentaGazetaRSS.ViewModels
{
    public class MainViewModel : Screen
    {
        private readonly INavigationService _navigationService;
        private IEnumerable<IRssItem> _items;
      
        public MainViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public IEnumerable<IRssItem> Items
        {
            get { return _items; }
            set
            {
                _items = value;
                NotifyOfPropertyChange(() => Items);
            }
        }

        public void ItemClick(object sender, ItemClickEventArgs e)
        {
            var item = e.ClickedItem as IRssItem;
            if (item != null)
            {
                _navigationService.NavigateToViewModel<RssItemViewModel>(item);
            }
        }

        protected override void OnActivate()
        {
            Debug.WriteLine("OnActivate");
            LoadFeed();
            base.OnActivate();
        }

        private async void LoadFeed()
        {
            if (Items == null)
            {
                if (Net.HasConnection())
                {
                    var mf = new MixedFeed();
                    Items = await mf.GetFeed();
                }
                else
                {
                    var dialog = new MessageDialog("Error Connection!");
                    dialog.Commands.Add(new UICommand("OK"));
                    await dialog.ShowAsync();
                }
            }
        }

    }
}