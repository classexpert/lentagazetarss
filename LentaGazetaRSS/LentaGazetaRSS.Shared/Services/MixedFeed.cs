﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LentaGazetaRSS.Models;
using LentaGazetaRSS.Models.RSS;

namespace LentaGazetaRSS.Services
{
    public class MixedFeed : BaseRssParser<LentaRssItem>
    {
        private readonly GazetaRssParser _gazetaParser = new GazetaRssParser();
        private readonly LentaRssParser _lentaParser = new LentaRssParser();

        public override async Task<IEnumerable<IRssItem>> GetFeed()
        {
            var tasklp = _lentaParser.GetFeed();
            var taskgp = _gazetaParser.GetFeed();

            await Task.WhenAll(tasklp, taskgp); // исходим из того, что оба ресурса всегда доступны

            return tasklp.Result.Concat(taskgp.Result).OrderByDescending(e => e.Date);
        }
    }
}