﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LentaGazetaRSS.Extensions;
using LentaGazetaRSS.Models;
using LentaGazetaRSS.Models.RSS;

namespace LentaGazetaRSS.Services
{
    public class LentaRssParser : BaseRssParser<LentaRssItem>
    {
        private readonly Uri _uri = new Uri("http://lenta.ru/rss");

        public override async Task<IEnumerable<IRssItem>> GetFeed()
        {
            var result = await GetRssXml(_uri);
            var rssFeed = result.Deserialize<Rss<LentaRssItem>>();

            return rssFeed.Collection.Items;
        }
    }
}