﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Storage;
using LentaGazetaRSS.Extensions;
using LentaGazetaRSS.Models;
using LentaGazetaRSS.Models.RSS;

namespace LentaGazetaRSS.Services
{
    public class TestRssParser : BaseRssParser<LentaRssItem>
    {
        public override async Task<IEnumerable<IRssItem>> GetFeed()
        {
            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Data/LentaRss.xml"));
            string result = await FileIO.ReadTextAsync(file);
            var rssFeed = result.Deserialize<Rss<LentaRssItem>>();

            return rssFeed.Collection.Items;
        }
    }
}