﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LentaGazetaRSS.Extensions;
using LentaGazetaRSS.Models;
using LentaGazetaRSS.Models.RSS;

namespace LentaGazetaRSS.Services
{
    public class GazetaRssParser : BaseRssParser<GazetaRssItem>
    {
        private readonly Uri _uri = new Uri("http://www.gazeta.ru/export/rss/lenta.xml");

        public override async Task<IEnumerable<IRssItem>> GetFeed()
        {
            var result = await GetRssXml(_uri);
            var rssFeed = result.Deserialize<Rss<GazetaRssItem>>();

            return rssFeed.Collection.Items;
        }
    }
}