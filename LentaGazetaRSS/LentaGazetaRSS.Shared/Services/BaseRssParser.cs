﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Web.Http;
using LentaGazetaRSS.Models.RSS;

namespace LentaGazetaRSS.Services
{
    public abstract class BaseRssParser<T>
    {
        public abstract Task<IEnumerable<IRssItem>> GetFeed();

        protected async Task<string> GetRssXml(Uri url)
        {
            using (var client = new HttpClient())   // стоит помнить про кеширование запросов, и решить, нужно ли оно нам
            {
                using (var response = await client.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        return await response.Content.ReadAsStringAsync();
                    }

                    return null;
                }
            }
        }
    }
}