﻿
using Windows.Networking.Connectivity;

namespace LentaGazetaRSS.Extensions
{
    public static class Net
    {
        public static bool HasConnection()
        {
            var connectionProfile = NetworkInformation.GetInternetConnectionProfile();
            return (connectionProfile != null &&
                    connectionProfile.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess);
        }
    }
}
